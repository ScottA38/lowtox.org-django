from django.urls import path, include
from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.random_detail_view, name='random'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('<int:question_id>/skip/', views.skip, name='skip'),
]
