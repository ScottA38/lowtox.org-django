from django.contrib import admin

from .models import Choice, Answer, Question, User


class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 2


class QuestionAdmin(admin.ModelAdmin):
    fields = ['question_text', 'type', 'label']
    inlines = [ChoiceInline]
    list_display = ('question_text', 'type')


class AnswerAdmin(admin.ModelAdmin):
    fields = ['user', 'question', 'answer_open', 'choice', 'skipped']
    list_display = ('user', 'question', 'get_answer')
    readonly_fields = ['user', 'question', 'answer_open', 'choice', 'skipped']
    search_fields = ['user__username', 'question__question_text', 'answer_open', 'choice__choice_text']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
