# Generated by Django 2.2.3 on 2019-07-30 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0014_auto_20190729_1522'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='answer',
            constraint=models.UniqueConstraint(fields=('question', 'user'), name='unique answer'),
        ),
    ]
