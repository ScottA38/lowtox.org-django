from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic
import random

from .models import Choice, Question, User, Answer, UserData


@login_required
def random_detail_view(request):
    user = auth.get_user(request)

    # retrieve the user's data
    if not hasattr(user, 'userdata'):
        # If it doesn't exist create it
        ud = UserData(user=user)
        ud.save()

    # get userdata__question
    question = user.userdata.question
    print(question)
    if(question):
        # if it exist return this question:
        context = {'question': question}
        return render(request, 'polls/random.html', context)
    else:
        # else return random question
        # get all questions answered by the user
        answered_questions = Question.objects.filter(answer__user__username=user.username)
        # get all questions unanswered by the user
        unanswered_questions = Question.objects.all().difference(answered_questions)
        # select a random one to display
        if len(unanswered_questions) > 0:
            rand = random.randint(0, len(unanswered_questions) - 1)
            q = unanswered_questions[rand]
            # store the question in the user's data
            user.userdata.question = q
            user.userdata.save()
            context = {'question': q}
            return render(request, 'polls/random.html', context)
        else:
            return render(request, 'polls/random.html')


@login_required
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    user = auth.get_user(request)
    if question.is_one_answer():
        return _handle_one_answer(question, user, request)
    elif question.is_multiple_answer():
        return _handle_multiple_answers(question, user, request)
    elif question.is_text() or question.is_number():
        return _handle_open_answer(question, user, request)


def _handle_one_answer(question, user, request):
    try:
        selected_choice = question.choice_set.get(pk=request.POST['answer'])
    except (KeyError, Choice.DoesNotExist):
        return HttpResponseRedirect(reverse('polls:random'))
    else:
        answer = Answer(question=question, user=user)
        answer.save()
        answer.choice.add(selected_choice)
        answer.save()
        user.userdata.question = None
        user.userdata.save()
        return HttpResponseRedirect(reverse('polls:random'))


def _handle_multiple_answers(question, user, request):
    selected_choices = question.choice_set.filter(pk__in=request.POST.getlist('answer'))
    if len(selected_choices) == 0:
        return HttpResponseRedirect(reverse('polls:random'))
    else:
        answer = Answer(question=question, user=user)
        answer.save()
        for selected_choice in selected_choices:
            answer.choice.add(selected_choice)
        answer.save()
        user.userdata.question = None
        user.userdata.save()
        return HttpResponseRedirect(reverse('polls:random'))


def _handle_open_answer(question, user, request):
    answer_open = request.POST['answer']
    if not answer_open:
        return HttpResponseRedirect(reverse('polls:random'))
    else:
        answer = Answer(question=question, user=user, answer_open=answer_open)
        answer.save()
        user.userdata.question = None
        user.userdata.save()
        return HttpResponseRedirect(reverse('polls:random'))


def skip(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    user = auth.get_user(request)
    answer = Answer(question=question, user=user, skipped=True)
    answer.save()
    user.userdata.question = None
    user.userdata.save()
    return HttpResponseRedirect(reverse('polls:random'))