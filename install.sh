#!/bin/sh

# In order to install from a tarball, run this script from the
# directory containing the extracted source with the --local flag
#
# sh install.sh --local

LOWTOX_GIT_REPO=https://gitlab.com/astralship/lowtox.org-django.git
DOCKER_APT_REPO=https://download.docker.com/linux/debian
INSTALL_DIR=/opt/lowtox-django
COMPOSE_FILE=docker-compose.yml

COMPOSE_VERSION=1.24.1

# Install docker
apt update

apt -y install \
	apt-transport-https \
	ca-certificates \
	gnupg2 \
	software-properties-common

wget https://download.docker.com/linux/debian/gpg -O- | apt-key add -

add-apt-repository \
	"deb [arch=amd64] $DOCKER_APT_REPO $(lsb_release -cs) stable"

apt update && apt -y install docker-ce docker-ce-cli containerd.io

# Start docker
service docker start

# Install docker-compose
url="https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose"
wget "$url-$(uname -s)-$(uname -m)" -O/usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

# Install git
apt install git

if [ "$1" != "--local"]
then
        # Clone the lowtox repo
        git clone $LOWTOX_GIT_REPO $INSTALL_DIR
        cd $INSTALL_DIR
fi

# Start services
docker-compose -f $COMPOSE_FILE up -d
