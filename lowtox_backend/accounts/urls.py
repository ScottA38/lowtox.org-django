from django.urls import path

from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name="accounts/login.html"), name='login'),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('token/<str:token>', views.login_token, name='login_token')
]